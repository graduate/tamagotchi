package com.gaurabdahal.tamagotchi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by gaurabdahal on 7/20/17.
 */
public class Tamagotchi {

    float gameStartTime;
    float curTime,secondTracker, secondTracker2;
    Context context;

    // States of a Bug
    enum State {
        Egg,
        EggCracking,
        Happy,
        Hungry,
        Eating,
    };

    boolean moveRight=true;
    State state;			// current state of tamagotchi
    int x,y;				// location on screen (in screen coordinates)
    double speed;			// speed of bug (in pixels per second)
    // All times are in seconds
    float timeToBirth;		// # seconds till birth
    float startBirthTimer;	// starting timestamp when decide to be born
    /*float deathTime;		// time of d0.eath*/
    float animateTimer;		// used to move and animate the bug
    Bitmap currentBm1,currentBm2;//=null;
    // Bug starts not alive
    public Tamagotchi (Context context) {
        state = State.Egg;
        this.context = context;
        currentBm1=null;
        currentBm2=null;
        secondTracker=secondTracker2=0;
    }

    public void init(Canvas canvas){
        y = (int) canvas.getHeight()/2;
        x = 0;
        animateTimer = System.nanoTime() / 1000000000f;
        currentBm1 = Assets.egg;
        currentBm2 = Assets.egg;
        state = State.Egg;
    }

    public void eggStage(Canvas canvas){
        if(state == State.Egg) {
            float currentTime = System.nanoTime();
            Log.e(Assets.DEBUGKEY,"time="+(currentTime - Assets.gameTimerProgress));
            if (MainThread.eggCracked) {
                state = State.EggCracking;
            }
        }
    }

    public void eggCrack(Canvas canvas){
        if(state == State.EggCracking) {
            currentBm1 = Assets.happy1;
            currentBm2 = Assets.happy2;
            Assets.lastFeedTime = System.nanoTime() / 1000000000f;
            Assets.totalHappinessCollected=Assets.happyDuration; // initially hatched tamagotchi is happy.
            state = State.Happy;
        }
    }

    public void happy(Canvas canvas){
        if(state==State.Happy){
            currentBm1 = Assets.happy1;
            currentBm2 = Assets.happy2;
            curTime = System.nanoTime() / 1000000000f;
            if(curTime - secondTracker >= 1){
                Assets.totalHappinessCollected--;
                secondTracker=curTime;
            }
            if(Assets.totalHappinessCollected<1){
                state = State.Hungry;
               // speak("I am Hungry");
            }

            //play hungry sound every 3 seconds
            curTime = System.nanoTime() / 1000000000f;
            if(curTime - secondTracker2 >= 3) {
                Assets.soundPool.play(Assets.sound_happy, 0.1f, 0.1f, 1, 0, 1);
                secondTracker2=curTime;
            }
        }
    }

    public void hungry(Canvas canvas){
        if(state==State.Hungry){
            currentBm1 = Assets.hungry1;
            currentBm2 = Assets.hungry2;

            //play hungry sound every 3 seconds
            curTime = System.nanoTime() / 1000000000f;
            if(curTime - secondTracker >= 3) {
                Assets.soundPool.play(Assets.sound_sad, 1, 1, 1, 0, 1);
                secondTracker=curTime;
            }
        }
    }

    public void feed(){
        if(Assets.totalHappinessCollected<Assets.maxHappinessDuration) {
            Assets.gameTimerProgress = System.nanoTime() / 1000000000f;

            Assets.totalHappinessCollected += Assets.happyDuration;
            if(Assets.totalHappinessCollected>Assets.maxHappinessDuration)
                Assets.totalHappinessCollected = Assets.maxHappinessDuration;
            state = State.Eating;
        }
    }

    public void eating(Canvas canvas){
        if(state == State.Eating){

            currentBm1 = Assets.eating1;
            currentBm2 = Assets.eating2;
            //show eating animation
            eatingAnimation(canvas);


            //play eating sound while eating
            float currentTime = System.nanoTime() / 1000000000f;
            if ((currentTime - Assets.gameTimerProgress <= Assets.eatingAnimationTime)) {
                if((int)( currentTime - Assets.gameTimerProgress) == Assets.eatingAnimationTime){
                    //play happy sound when done eating
                    //Assets.soundPool.play(Assets.sound_happy, 0.5f, 0.5f, 1, 0, 1);

                } else{
                    //play hungry sound every 3 seconds
                    if(currentTime - secondTracker >1) {
                        if(Assets.is_sound_loaded)
                            Assets.soundPool.play(Assets.sound_eating, 0.7f, 0.7f, 1, 0, 1);
                        secondTracker=currentTime;
                    }
                }
            }else{
                //speak("I am Very Happy");
                state = State.Happy;
                Assets.gameTimerProgress = currentTime;
                Assets.lastFeedTime =currentTime;
            }

            //play hungry sound every 3 seconds

        }
    }

    public void setSpeed(Canvas canvas){
        if(state == State.Happy || state == State.Egg) speed = canvas.getWidth() / 4; // no faster than 1/4 a screen per second
        else if(state == State.Hungry) speed = canvas.getWidth() / 8; // no faster than 1/8 a screen per second
    }

    public void move(Canvas canvas){
        Log.e(Assets.DEBUGKEY,"State ="+state+", totalHappiness="+Assets.totalHappinessCollected);
        if(state == State.Eating){

        }else {
            float curTime = System.nanoTime() / 1000000000f;
            float elapsedTime = curTime - animateTimer;
            animateTimer = curTime;
            // Compute the amount of pixels to move (vertically down the screen)
            if (moveRight) {
                x += (speed * elapsedTime);
            } else {
                x -= (speed * elapsedTime);
            }
            if (x >= canvas.getWidth()) {
                x = canvas.getWidth();
                moveRight = false;
            }
            if (x <currentBm1.getWidth()) {
                x = currentBm1.getWidth();
                moveRight = true;
            }
            // Draw tamagotchi on screen
            curTime = System.currentTimeMillis()/ 100 %10;
            if(curTime%2==0) {
                canvas.drawBitmap(currentBm1, x - currentBm1.getWidth(), y - currentBm1.getHeight() / 2, null);
            }else{
                canvas.drawBitmap(currentBm2, x - currentBm2.getWidth(), y - currentBm2.getHeight() / 2, null);
            }
        }
    }

    public void eatingAnimation(Canvas canvas){
        float curTime = System.currentTimeMillis()/ 100 %10;
        Log.e(Assets.DEBUGKEY,curTime+"");
        if(curTime%2==0) {
            canvas.drawBitmap(Assets.eating1, x - Assets.eating1.getWidth(), y - Assets.eating1.getHeight() / 2, null);
        }else{
            canvas.drawBitmap(Assets.eating2, x - Assets.eating2.getWidth
                    (), y - Assets.eating2.getHeight() / 2, null);
        }
    }

    public void speak(String toSpeak) {
        // Display the text as a Toast
       // Toast.makeText(context, toSpeak, Toast.LENGTH_SHORT).show();
        // Speak the text
        if (MainActivity.tts_initialized) {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP)
                MainActivity.tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            else
                MainActivity.tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, "1");
        }
    }
}


