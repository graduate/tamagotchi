package com.gaurabdahal.tamagotchi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by gaurabdahal on 7/20/17.
 */
public class SplashActivity extends AppCompatActivity{
    boolean quit;
    Bitmap bmp;

    private Handler mHandler = new Handler();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        quit = false;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!quit) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }
        }, 3000);

    }

    @Override
    public void onBackPressed(){
        quit = true;
        super.onBackPressed();
    }


}
