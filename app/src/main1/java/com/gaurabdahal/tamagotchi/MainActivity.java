package com.gaurabdahal.tamagotchi;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    MainView v;

    //---------
    public  static TextToSpeech tts;
    public static boolean tts_initialized;
    //---------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature (Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        v = new MainView(this);
        setContentView(v);

        // Create the TTS object
       /* tts_initialized = false;
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    // Set the language to use
                    tts.setLanguage(Locale.UK);
                    tts_initialized = true;
                }
            }
        });*/
    }

    /*___________________________________________________________________
|
| Function: onResume
|__________________________________________________________________*/
    @Override
    protected void onResume () {
        // Kill the notification, if any
        Intent intent = new Intent(this, GatorService.class);
        intent.setAction(GatorService.ACTION_KILL_NOTIFICATION);
        startService(intent);

        super.onResume();


        if (v != null)
            v.resume();

    }

    /*___________________________________________________________________
  |
  | Function: onPause
  |__________________________________________________________________*/
    @Override
    protected void onPause () {
        /*   if ( !isFinishing()) {
            // Allow the gator to get hungry in 5 seconds
         Log.e(Assets.DEBUGKEY, "Onpause MainActivity");
            float curTime = System.nanoTime() / 1000000000f;
            Assets.timeWhenHungry = curTime + 5;
            Intent intent = new Intent(this, GatorService.class);
            intent.setAction(GatorService.ACTION_START);
            startService(intent);
        }*/
        // Call super class version
        super.onPause();


        //-- from bugsmasher
        if (v != null)
            v.pause();
    }

}
