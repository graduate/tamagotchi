package com.gaurabdahal.tamagotchi;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.SoundPool;

/**
 * Created by gaurabdahal on 7/20/17.
 */
public class Assets {
    public static String DEBUGKEY = "TAMAGOTCHI";
    public static MediaPlayer mp;
    public static float timeWhenHungry;	// a timestamp when the gator will be hungry
    public static float timeWhenDoneEating;
    static boolean is_sound_loaded=false;

    static SoundPool soundPool;
    static int sound_happy;
    static int sound_sad;
    static int sound_eating;
    static int sound_crack;

    // True when the service is processing a wait timer
    public static volatile boolean serviceIsProcessing;

    //Bitmaps used in app
    static Bitmap background;   //background image
    static Bitmap egg;          //egg state image
    static Bitmap happy1;
    static Bitmap happy2;       //happy image
    static Bitmap hungry1;
    static Bitmap hungry2;      //sad image
    static Bitmap eating1;      //eating image 1 for animation
    static Bitmap eating2;      //eating image 2 for animation
    static Bitmap food;
    static Bitmap option;

    static Bitmap currentBm;


    // States of the Game Screen
    enum GameState {
        Starting,
        Running,
        Eating,
    };

    static GameState state;
    static float gameTimerProgress;
    static float lastFeedTime;
    static int totalHappinessCollected=0;
    static int maxHappinessDuration=120;
    static int eatingAnimationTime = 5;

    static int hatchingTime = 7; //hatch after 7 seconds of being egg;
    static int happyDuration = 15; //happy for 10 seconds;

    static Tamagotchi tamagotchi;
}
