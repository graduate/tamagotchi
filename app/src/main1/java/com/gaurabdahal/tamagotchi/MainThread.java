package com.gaurabdahal.tamagotchi;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by gaurabdahal on 7/20/17.
 */
public class MainThread extends Thread {
    private SurfaceHolder holder;
    private Handler handler;		// required for running code in the UI thread
    private boolean isRunning = false;
    Context context;
    Paint paint;
    int touchx, touchy;	// x,y of touch event
    boolean touched;
    static boolean eggCracked;	// true if touch happened
    boolean data_initialized;
    int tx,ty;
    int num_sounds_loaded=0;



    private static final Object lock = new Object();

    public MainThread (SurfaceHolder surfaceHolder, Context context) {
        holder = surfaceHolder;
        this.context = context;
        handler = new Handler();
        data_initialized = false;
        touched = false;
        eggCracked=false;
        tx=ty=0;




        //----------
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Assets.soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        }
        else {
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            Assets.soundPool = new SoundPool.Builder()
                    .setAudioAttributes(attributes)
                    .build();

        }

        Assets.sound_happy = Assets.soundPool.load(context, R.raw.winner, 1);
        Assets.sound_sad = Assets.soundPool.load(context, R.raw.growling, 1);
        Assets.sound_eating = Assets.soundPool.load(context, R.raw.eating, 1);
        Assets.sound_crack = Assets.soundPool.load(context, R.raw.crack, 1);

        Assets.soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener(){
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                num_sounds_loaded++;
                if(num_sounds_loaded==4){
                    Assets.is_sound_loaded=true;
                }
            }
        });

    }

    public void setRunning(boolean b) {
        isRunning = b;	// no need to synchronize this since this is the only line of code to writes this variable
    }

    @Override
    public void run() {
        while (isRunning) {
            // Lock the canvas before drawing
            Canvas canvas = holder.lockCanvas();
            if (canvas != null) {
                // Perform drawing operations on the canvas
                render(canvas);

                holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    private void render (Canvas canvas) {
        int i, x, y;

        if (!data_initialized) {
            loadData(canvas);
            data_initialized = true;
        }

        switch (Assets.state) {
            case Starting:
                Log.e(Assets.DEBUGKEY, "Game screen with egg");
                loadBackground(canvas, R.drawable.background);
                canvas.drawBitmap(Assets.background, 0, 0, null);
                Assets.state = Assets.GameState.Running;
                Assets.tamagotchi.init(canvas);
                Assets.gameTimerProgress = System.nanoTime()/1000000000f;
                break;
            case Running:
                canvas.drawBitmap(Assets.background, 0, 0, null);
                canvas.drawBitmap(Assets.option, canvas.getWidth()-Assets.option.getWidth()-50, 20, null);

                if(eggCracked)
                    if(Assets.tamagotchi.state != Tamagotchi.State.Eating) {
                        canvas.drawBitmap(Assets.food, 50, 10, null);
                    }
                float currentTime = System.nanoTime() / 1000000000f;
                if (!eggCracked && (currentTime - Assets.gameTimerProgress >= Assets.hatchingTime)) {
                    if(Assets.is_sound_loaded)
                        Assets.soundPool.play(Assets.sound_crack, 1, 1, 1, 0, 1);
                    eggCracked = true;
                }

                if(touched){
                    if(foodTouched(canvas,tx,ty)){
                        Assets.tamagotchi.feed();
                    }
                    if(optionTouched(canvas,tx,ty)){

                    }
                    touched=false;
                }
                break;
        }
        Assets.tamagotchi.eggStage(canvas);
        Assets.tamagotchi.eggCrack(canvas);
        Assets.tamagotchi.happy(canvas);
        Assets.tamagotchi.hungry(canvas);
        Assets.tamagotchi.eating(canvas);
        Assets.tamagotchi.setSpeed(canvas);
        Assets.tamagotchi.move(canvas);
    }



    // Load specific background screen
    private void loadBackground (Canvas canvas, int resId) {
        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), resId);
        Assets.background = Bitmap.createScaledBitmap (bmp, canvas.getWidth(), canvas.getHeight(), false);
        bmp = null;
    }

    public void touch(int x, int y){
        tx=x;
        ty=y;
        touched=true;
    }

    public boolean foodTouched (Canvas canvas, int touchx, int touchy) {
        boolean touched = false;
            // Compute distance between touch and center of bug
            float distance = (float)(Math.sqrt ((touchx - (50+Assets.food.getWidth()/2)) * (touchx - (50+Assets.food.getWidth()/2)) + (touchy - (10+Assets.food.getHeight()/2)) * (touchy - (10+Assets.food.getHeight()/2))));
            // Is this close enough for a kill?
            if (distance <= Assets.food.getWidth()*0.5f) {
                //make sure Tamagotchi is not in eating state
                if(Assets.tamagotchi.state != Tamagotchi.State.Eating) {
                    Log.e(Assets.DEBUGKEY, "You Touched food");
                    touched = true;
                }
            }
        return (touched);
    }

    public boolean optionTouched (Canvas canvas, int touchx, int touchy) {
        boolean touched = false;
        float dis = (float)(Math.sqrt (Math.pow((touchx - (canvas.getWidth()-50-Assets.option.getWidth()/2)),2) + Math.pow((touchy - (20+Assets.option.getHeight()/2)),2)));
        // Is this close enough for a kill?
        if (dis <= Assets.option.getWidth()*0.5f) {
            Intent mIntent = new Intent(context,PrefsActivity.class);
            context.startActivity(mIntent);
            touched=true;
        }
        return (touched);
    }

    // Loads graphics, etc. used in game
    private void loadData (Canvas canvas) {
        Bitmap bmp;
        int newWidth, newHeight;
        float scaleFactor;

        // Create a paint object for drawing vector graphics
        paint = new Paint();


        // Load egg
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.egg);
        newWidth = (int)(canvas.getWidth() * 0.2f);
        scaleFactor = (float)newWidth / bmp.getWidth();
        newHeight = (int)(bmp.getHeight() * scaleFactor);
        Assets.egg = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        bmp = null;



        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.food);
        newWidth = (int)(canvas.getWidth() * 0.15f);
        scaleFactor = (float)newWidth / bmp.getWidth();
        newHeight = (int)(bmp.getHeight() * scaleFactor);
        Assets.food = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;


        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.option);
        newWidth = (int)(canvas.getWidth() * 0.1f);
        scaleFactor = (float)newWidth / bmp.getWidth();
        newHeight = (int)(bmp.getHeight() * scaleFactor);
        Assets.option = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;


        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.t_eating1);
        newWidth = (int)(canvas.getWidth() * 0.2f);
        scaleFactor = (float)newWidth / bmp.getWidth();
        newHeight = (int)(bmp.getHeight() * scaleFactor);
        Assets.eating1 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;

        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.t_eating2);
        Assets.eating2 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;

        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.happy1);
        Assets.happy1 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;

        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.happy2);
        Assets.happy2 = Bitmap.createScaledBitmap(bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;

        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.hungry1);
        Assets.hungry1 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;

        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.hungry2);
        Assets.hungry2 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;

        // Create a tamagotchi
        Assets.tamagotchi = new Tamagotchi(context);
    }


}
