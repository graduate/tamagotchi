package com.gaurabdahal.tamagotchi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;

/**
 * Created by gaurabdahal on 7/11/17.
 */
public class PrefsFragmentSettings  extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    Context context;

    public PrefsFragmentSettings(Context context){
        this.context = context;

    }

    public PrefsFragmentSettings(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs_fragment_settings);
    }

    public void onResume(){
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        Preference prefs;
        prefs = getPreferenceScreen().findPreference("key_restart_game");
        prefs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                Intent intent = new Intent(context, SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //this will always start your activity as a new task
                startActivity(intent);
        return true;
            }
        });


    }

    public void onPause(){
        super.onPause();

    }

    /**
     * Called when a shared preference is changed, added, or removed. This
     * may be called even if a preference is set to its existing value.
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }
}
