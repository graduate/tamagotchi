package com.gaurabdahal.tamagotchi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by gaurabdahal on 7/20/17.
 */
public class SplashActivity extends AppCompatActivity{
    Bitmap bmp;
    private Handler mHandler = new Handler();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
       // startActivity(new Intent(SplashActivity.this, MainActivity.class));
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();*/

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 1000);

    }
/*
    public class MyView extends View {
        boolean switchToGameScreen;
        public MyView(Context context) {
            super(context);
            switchToGameScreen = false;
        }
        @Override
        protected void onDraw (Canvas canvas) {
            // Load image if needed
            if (bmp == null)
                bmp = BitmapFactory.decodeResource(getResources(), R.drawable.splashscreen);
            // Draw the title full screen
            Rect dstRect = new Rect();
            canvas.getClipBounds(dstRect);
            canvas.drawBitmap(bmp, null, dstRect, null);
            bmp = null;
            invalidate();
        }
    }*/

}
